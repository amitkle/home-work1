#include "stack.h"

void initStack(myStack *s)
{	
	init(&(s->list));
}
void cleanStack(myStack *s){
	while(pop(s) != -1)
	{
	}
	delete s;
}

bool isEmpty(myStack *s){
	return (s->list).size == 0;
}
// insert element to top of the stack
void push(myStack *s, int element){
	add(&(s->list),element);
 }
//remove element from top of the stack
int pop(myStack *s){
    int ans = -1;
	if (!isEmpty(s))
	{
		ans = remove(&(s->list),0);
    }
    return ans;
}

