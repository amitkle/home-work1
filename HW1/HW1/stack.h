#include "linkedList.h"

struct myStack{
	myLinkedList list;
};
typedef struct myStack myStack;


void push(myStack *s, int element);
int pop(myStack *s) ;
void initStack(myStack *s);
void cleanStack(myStack *s);
bool isEmpty(myStack *s);
