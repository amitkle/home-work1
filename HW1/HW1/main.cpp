#include "queue.h"
#include <stdio.h>
#include <iostream>
using namespace std;
#define LEN 10

void reverse(int* nums, int size) 
{
	myStack s;
	initStack(&s);
	int i;
	for(i=0;i<size;i++)
	{
		push(&s,*(nums+i));
	}
	for(i=0;i<size;i++)
	{
		*(nums+i) = pop(&s);
	}
}
int main(){
	//task 1:
	/*
	int arr[LEN];
	int i;
	cout << "Please enter 10 integers" << endl;
	for(i = 0;i < LEN;i++)
	{
		cin >> arr[i];
	}
	reverse(arr,LEN);
	for(i=0;i<LEN;i++)
	{
		cout << arr[i] << endl;
	}*/

	//task 2:
	myQueue q;
	int i;
	initQueue(&q);
	for(i = 15 ; i < 20 ; i++)
	{
		enqueue(&q , i);
	}
	for(i = 15 ; i < 20 ; i++)
	{
		cout << dequeue(&q) << endl;
	}
	return 0;
}