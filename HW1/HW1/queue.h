#include "stack.h"


struct myQueue{
	myStack s;
};
typedef struct myQueue myQueue;

void enqueue(myQueue *q,int num);
int dequeue(myQueue *q);
void initQueue(myQueue *q);
int getSize(myQueue *q);