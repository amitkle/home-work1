typedef struct myNode{
	int num;
	myNode* next;
}myNode;


struct myLinkedList{
	myNode *head;
	int size;

};
typedef struct myLinkedList myLinkedList;

void add(myLinkedList *l, int num);
int remove(myLinkedList *l, int index);//-1 for empty;
void init(myLinkedList *l);