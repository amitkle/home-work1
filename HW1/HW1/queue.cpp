#include "queue.h"

void enqueue(myQueue *q,int num)
{
	push(&(q->s),num);
}
int dequeue(myQueue *q)
{
	int i, val = -1;
	myStack s;
	initStack(&s);
	for(i = 0; getSize(q) != 0 ;i++)
	{
		push(&s,pop(&(q->s)));
	}
	val = pop(&s);
	for(; i > 1 ; i--)
	{
		push(&(q->s),pop(&s));
	}
	return val;
}
void initQueue(myQueue *q)
{
	initStack(&(q->s));
}
int getSize(myQueue *q)
{
	return q->s.list.size;
}