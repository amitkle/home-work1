struct myQueue{
	int maxSize;
	int count;
	bool empty;
	int* elements;
};
typedef struct myQueue myQueue;

void enqueue(myQueue *q, int element);
int dequeue(myQueue *q);
void initQueue(myQueue *q, int size);
void cleanQueue(myQueue *q);
bool isEmpty(myQueue *q);
bool isFull(myQueue *q);
