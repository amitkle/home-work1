#include "myQueue.h"

void initQueue(myQueue *q, int size)
{
	q->count = 0;
	q->empty = true;
	q->maxSize = size;
	q->elements = new int[q->maxSize];
}
void enqueue(myQueue *q, int element)
{
	if(!isFull(q))
	{
		q->elements[q->count] = element;
		q->count++;
	}
}
int dequeue(myQueue *q)
{
	int ans = -1, i;
	if(!isEmpty(q))
	{
		q->count--;
		ans = q->elements[0];
		for(i = 0; i < q->count ; i++)
		{
			q->elements[i] = q->elements[i+1];
		}
		q->empty =  (q->count == 0);
	}
}
void cleanQueue(myQueue *q)
{
	delete[] q;
}
bool isEmpty(myQueue *q)
{
	return q->empty;
}
bool isFull(myQueue *q)
{
	return (q->count == q->maxSize);
}