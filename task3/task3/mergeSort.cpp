#include <iostream>
using namespace std;
#define LENGTH 10

void mergeSort(int* arr,int begin, int end);
void mergeTwoArrays(int* arr,int begin, int mid, int end);
int main()
{
	int arr[] = {7,2,1,3,4,10,9,5,8,6};
	mergeSort(arr, 0, LENGTH - 1);
	int i;
	for(i=0;i<LENGTH;i++)
	{
		std::cout << arr[i] << endl;
	}
	return 0;
}
void mergeTwoArrays(int* arr,int begin, int mid, int end)
{
	int *arr2 = new int[(end + 1)-begin];
	int h = begin, j = mid + 1, i = 0,k;
	while(h <= mid && j <= end)
	{
		if(arr[h] < arr[j])
		{
			arr2[i++] = arr[h++]; 
		}
		else
		{
			arr2[i++] = arr[j++];
		}
	}

	if(h>mid)
	{
		for(k=j;k<=end;k++)
		{
			arr2[i]=arr[k];
			i++;
		}
	}
	else
	{
		for(k=h;k<=mid;k++)
		{
			arr2[i]=arr[k];
			i++;
		}
	}

	for(i=0;i<=end-begin;i++) 
	{
		arr[i+begin]=arr2[i];
	}
	delete[] arr2;
}
void mergeSort(int* arr,int begin, int end)
{
	int mid;
	if(begin < end)
	{
		mid=(begin+end)/2;
		mergeSort(arr, begin,mid);
		mergeSort(arr, mid+1,end);
		mergeTwoArrays(arr, begin,mid,end);
	}
}
